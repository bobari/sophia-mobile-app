import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { Q1Q4Page } from './q1-q4.page';

const routes: Routes = [
  {
    path: '',
    component: Q1Q4Page
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [Q1Q4Page]
})
export class Q1Q4PageModule {}
