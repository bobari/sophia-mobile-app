import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustJoinUsPage } from './cust-join-us.page';

const routes: Routes = [
  {
    path: '',
    component: CustJoinUsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustJoinUsPage]
})
export class CustJoinUsPageModule {}
