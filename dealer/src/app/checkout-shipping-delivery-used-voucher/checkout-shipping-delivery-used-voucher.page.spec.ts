import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckoutShippingDeliveryUsedVoucherPage } from './checkout-shipping-delivery-used-voucher.page';

describe('CheckoutShippingDeliveryUsedVoucherPage', () => {
  let component: CheckoutShippingDeliveryUsedVoucherPage;
  let fixture: ComponentFixture<CheckoutShippingDeliveryUsedVoucherPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckoutShippingDeliveryUsedVoucherPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckoutShippingDeliveryUsedVoucherPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
