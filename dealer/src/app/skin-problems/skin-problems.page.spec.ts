import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkinProblemsPage } from './skin-problems.page';

describe('SkinProblemsPage', () => {
  let component: SkinProblemsPage;
  let fixture: ComponentFixture<SkinProblemsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SkinProblemsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkinProblemsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
