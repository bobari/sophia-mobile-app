import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CheckoutPaymentUsePointsPage } from './checkout-payment-use-points.page';

const routes: Routes = [
  {
    path: '',
    component: CheckoutPaymentUsePointsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CheckoutPaymentUsePointsPage]
})
export class CheckoutPaymentUsePointsPageModule {}
