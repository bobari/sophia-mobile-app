import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SelectDeliveryAddressPage } from './select-delivery-address.page';

const routes: Routes = [
  {
    path: '',
    component: SelectDeliveryAddressPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SelectDeliveryAddressPage]
})
export class SelectDeliveryAddressPageModule {}
