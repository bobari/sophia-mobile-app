import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cust-package-details-added-to-cart',
  templateUrl: './cust-package-details-added-to-cart.page.html',
  styleUrls: ['./cust-package-details-added-to-cart.page.scss'],
})
export class CustPackageDetailsAddedToCartPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
