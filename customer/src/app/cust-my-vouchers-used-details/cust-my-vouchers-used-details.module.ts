import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustMyVouchersUsedDetailsPage } from './cust-my-vouchers-used-details.page';

const routes: Routes = [
  {
    path: '',
    component: CustMyVouchersUsedDetailsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustMyVouchersUsedDetailsPage]
})
export class CustMyVouchersUsedDetailsPageModule {}
