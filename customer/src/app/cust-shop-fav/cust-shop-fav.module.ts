import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustShopFavPage } from './cust-shop-fav.page';

const routes: Routes = [
  {
    path: '',
    component: CustShopFavPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustShopFavPage]
})
export class CustShopFavPageModule {}
