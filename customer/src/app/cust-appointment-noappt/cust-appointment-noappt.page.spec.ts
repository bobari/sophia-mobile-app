import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustAppointmentNoapptPage } from './cust-appointment-noappt.page';

describe('CustAppointmentNoapptPage', () => {
  let component: CustAppointmentNoapptPage;
  let fixture: ComponentFixture<CustAppointmentNoapptPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustAppointmentNoapptPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustAppointmentNoapptPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
