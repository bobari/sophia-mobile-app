import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RegisterThreeFivePage } from './register-three-five.page';

const routes: Routes = [
  {
    path: '',
    component: RegisterThreeFivePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RegisterThreeFivePage]
})
export class RegisterThreeFivePageModule {}
