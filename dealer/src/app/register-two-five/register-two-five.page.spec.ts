import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterTwoFivePage } from './register-two-five.page';

describe('RegisterTwoFivePage', () => {
  let component: RegisterTwoFivePage;
  let fixture: ComponentFixture<RegisterTwoFivePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterTwoFivePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterTwoFivePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
