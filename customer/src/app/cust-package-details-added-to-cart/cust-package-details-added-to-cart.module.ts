import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustPackageDetailsAddedToCartPage } from './cust-package-details-added-to-cart.page';

const routes: Routes = [
  {
    path: '',
    component: CustPackageDetailsAddedToCartPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustPackageDetailsAddedToCartPage]
})
export class CustPackageDetailsAddedToCartPageModule {}
