import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-checkout-shipping-self-collect',
  templateUrl: './checkout-shipping-self-collect.page.html',
  styleUrls: ['./checkout-shipping-self-collect.page.scss'],
})
export class CheckoutShippingSelfCollectPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
