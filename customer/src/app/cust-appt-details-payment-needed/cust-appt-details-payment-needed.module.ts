import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustApptDetailsPaymentNeededPage } from './cust-appt-details-payment-needed.page';

const routes: Routes = [
  {
    path: '',
    component: CustApptDetailsPaymentNeededPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustApptDetailsPaymentNeededPage]
})
export class CustApptDetailsPaymentNeededPageModule {}
