import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustUpcomingApptPage } from './cust-upcoming-appt.page';

const routes: Routes = [
  {
    path: '',
    component: CustUpcomingApptPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustUpcomingApptPage]
})
export class CustUpcomingApptPageModule {}
