import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustSplashScreenPage } from './cust-splash-screen.page';

const routes: Routes = [
  {
    path: '',
    component: CustSplashScreenPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustSplashScreenPage]
})
export class CustSplashScreenPageModule {}
