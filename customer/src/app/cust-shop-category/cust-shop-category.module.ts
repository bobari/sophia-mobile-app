import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustShopCategoryPage } from './cust-shop-category.page';

const routes: Routes = [
  {
    path: '',
    component: CustShopCategoryPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustShopCategoryPage]
})
export class CustShopCategoryPageModule {}
