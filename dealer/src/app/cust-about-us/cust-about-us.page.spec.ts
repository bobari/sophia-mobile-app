import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustAboutUsPage } from './cust-about-us.page';

describe('CustAboutUsPage', () => {
  let component: CustAboutUsPage;
  let fixture: ComponentFixture<CustAboutUsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustAboutUsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustAboutUsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
