import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SummarySkinProblemsPage } from './summary-skin-problems.page';

const routes: Routes = [
  {
    path: '',
    component: SummarySkinProblemsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SummarySkinProblemsPage]
})
export class SummarySkinProblemsPageModule {}
