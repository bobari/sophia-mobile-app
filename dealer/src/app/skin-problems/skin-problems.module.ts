import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SkinProblemsPage } from './skin-problems.page';

const routes: Routes = [
  {
    path: '',
    component: SkinProblemsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SkinProblemsPage]
})
export class SkinProblemsPageModule {}
