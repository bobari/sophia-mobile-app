import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { OrderDetailsSelfCollectClosedPage } from './order-details-self-collect-closed.page';

const routes: Routes = [
  {
    path: '',
    component: OrderDetailsSelfCollectClosedPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [OrderDetailsSelfCollectClosedPage]
})
export class OrderDetailsSelfCollectClosedPageModule {}
