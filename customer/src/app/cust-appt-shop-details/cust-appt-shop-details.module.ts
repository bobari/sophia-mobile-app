import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustApptShopDetailsPage } from './cust-appt-shop-details.page';

const routes: Routes = [
  {
    path: '',
    component: CustApptShopDetailsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustApptShopDetailsPage]
})
export class CustApptShopDetailsPageModule {}
