import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { Q18Q20Page } from './q18-q20.page';

const routes: Routes = [
  {
    path: '',
    component: Q18Q20Page
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [Q18Q20Page]
})
export class Q18Q20PageModule {}
