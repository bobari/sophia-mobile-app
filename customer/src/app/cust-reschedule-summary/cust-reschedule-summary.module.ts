import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustRescheduleSummaryPage } from './cust-reschedule-summary.page';

const routes: Routes = [
  {
    path: '',
    component: CustRescheduleSummaryPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustRescheduleSummaryPage]
})
export class CustRescheduleSummaryPageModule {}
