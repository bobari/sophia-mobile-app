import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustApptDeletedPage } from './cust-appt-deleted.page';

const routes: Routes = [
  {
    path: '',
    component: CustApptDeletedPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustApptDeletedPage]
})
export class CustApptDeletedPageModule {}
