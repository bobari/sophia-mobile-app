import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustAlbumPage } from './cust-album.page';

describe('CustAlbumPage', () => {
  let component: CustAlbumPage;
  let fixture: ComponentFixture<CustAlbumPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustAlbumPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustAlbumPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
