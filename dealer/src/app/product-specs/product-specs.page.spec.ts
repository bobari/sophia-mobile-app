import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductSpecsPage } from './product-specs.page';

describe('ProductSpecsPage', () => {
  let component: ProductSpecsPage;
  let fixture: ComponentFixture<ProductSpecsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductSpecsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductSpecsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
