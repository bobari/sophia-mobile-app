import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetLanguagePage } from './set-language.page';

describe('SetLanguagePage', () => {
  let component: SetLanguagePage;
  let fixture: ComponentFixture<SetLanguagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetLanguagePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetLanguagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
