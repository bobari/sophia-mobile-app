import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustPurchasedPackageDetailsPage } from './cust-purchased-package-details.page';

const routes: Routes = [
  {
    path: '',
    component: CustPurchasedPackageDetailsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustPurchasedPackageDetailsPage]
})
export class CustPurchasedPackageDetailsPageModule {}
