import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CheckoutShippingDeliveryApplyCodePage } from './checkout-shipping-delivery-apply-code.page';

const routes: Routes = [
  {
    path: '',
    component: CheckoutShippingDeliveryApplyCodePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CheckoutShippingDeliveryApplyCodePage]
})
export class CheckoutShippingDeliveryApplyCodePageModule {}
