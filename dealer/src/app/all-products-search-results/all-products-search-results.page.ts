import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-all-products-search-results',
  templateUrl: './all-products-search-results.page.html',
  styleUrls: ['./all-products-search-results.page.scss'],
})
export class AllProductsSearchResultsPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
