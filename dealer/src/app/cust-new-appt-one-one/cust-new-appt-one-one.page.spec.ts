import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustNewApptOneOnePage } from './cust-new-appt-one-one.page';

describe('CustNewApptOneOnePage', () => {
  let component: CustNewApptOneOnePage;
  let fixture: ComponentFixture<CustNewApptOneOnePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustNewApptOneOnePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustNewApptOneOnePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
