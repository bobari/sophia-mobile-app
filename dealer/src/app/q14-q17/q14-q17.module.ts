import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { Q14Q17Page } from './q14-q17.page';

const routes: Routes = [
  {
    path: '',
    component: Q14Q17Page
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [Q14Q17Page]
})
export class Q14Q17PageModule {}
