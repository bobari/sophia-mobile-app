import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustNewApptOneTwoPage } from './cust-new-appt-one-two.page';

const routes: Routes = [
  {
    path: '',
    component: CustNewApptOneTwoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustNewApptOneTwoPage]
})
export class CustNewApptOneTwoPageModule {}
