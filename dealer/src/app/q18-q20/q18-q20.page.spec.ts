import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Q18Q20Page } from './q18-q20.page';

describe('Q18Q20Page', () => {
  let component: Q18Q20Page;
  let fixture: ComponentFixture<Q18Q20Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Q18Q20Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Q18Q20Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
