import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustSettingsHelpTipsResetPage } from './cust-settings-help-tips-reset.page';

const routes: Routes = [
  {
    path: '',
    component: CustSettingsHelpTipsResetPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustSettingsHelpTipsResetPage]
})
export class CustSettingsHelpTipsResetPageModule {}
