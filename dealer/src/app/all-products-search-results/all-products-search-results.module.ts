import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AllProductsSearchResultsPage } from './all-products-search-results.page';

const routes: Routes = [
  {
    path: '',
    component: AllProductsSearchResultsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AllProductsSearchResultsPage]
})
export class AllProductsSearchResultsPageModule {}
