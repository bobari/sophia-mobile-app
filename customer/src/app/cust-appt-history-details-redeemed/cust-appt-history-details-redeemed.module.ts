import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustApptHistoryDetailsRedeemedPage } from './cust-appt-history-details-redeemed.page';

const routes: Routes = [
  {
    path: '',
    component: CustApptHistoryDetailsRedeemedPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustApptHistoryDetailsRedeemedPage]
})
export class CustApptHistoryDetailsRedeemedPageModule {}
