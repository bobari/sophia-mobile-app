import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustNewApptThreePage } from './cust-new-appt-three.page';

const routes: Routes = [
  {
    path: '',
    component: CustNewApptThreePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustNewApptThreePage]
})
export class CustNewApptThreePageModule {}
