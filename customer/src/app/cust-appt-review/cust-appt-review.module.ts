import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustApptReviewPage } from './cust-appt-review.page';

const routes: Routes = [
  {
    path: '',
    component: CustApptReviewPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustApptReviewPage]
})
export class CustApptReviewPageModule {}
