import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustCashVoucherDetailsPage } from './cust-cash-voucher-details.page';

const routes: Routes = [
  {
    path: '',
    component: CustCashVoucherDetailsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustCashVoucherDetailsPage]
})
export class CustCashVoucherDetailsPageModule {}
