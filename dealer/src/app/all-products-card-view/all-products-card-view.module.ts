import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AllProductsCardViewPage } from './all-products-card-view.page';

const routes: Routes = [
  {
    path: '',
    component: AllProductsCardViewPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AllProductsCardViewPage]
})
export class AllProductsCardViewPageModule {}
