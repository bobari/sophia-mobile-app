import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustApptDeletedPage } from './cust-appt-deleted.page';

describe('CustApptDeletedPage', () => {
  let component: CustApptDeletedPage;
  let fixture: ComponentFixture<CustApptDeletedPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustApptDeletedPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustApptDeletedPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
