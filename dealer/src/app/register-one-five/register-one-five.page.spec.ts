import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterOneFivePage } from './register-one-five.page';

describe('RegisterOneFivePage', () => {
  let component: RegisterOneFivePage;
  let fixture: ComponentFixture<RegisterOneFivePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterOneFivePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterOneFivePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
