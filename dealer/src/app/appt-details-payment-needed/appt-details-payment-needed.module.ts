import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ApptDetailsPaymentNeededPage } from './appt-details-payment-needed.page';

const routes: Routes = [
  {
    path: '',
    component: ApptDetailsPaymentNeededPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ApptDetailsPaymentNeededPage]
})
export class ApptDetailsPaymentNeededPageModule {}
