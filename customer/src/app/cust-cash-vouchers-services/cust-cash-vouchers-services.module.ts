import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustCashVouchersServicesPage } from './cust-cash-vouchers-services.page';

const routes: Routes = [
  {
    path: '',
    component: CustCashVouchersServicesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustCashVouchersServicesPage]
})
export class CustCashVouchersServicesPageModule {}
