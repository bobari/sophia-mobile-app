import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentChooseCreditDebitCardPage } from './payment-choose-credit-debit-card.page';

describe('PaymentChooseCreditDebitCardPage', () => {
  let component: PaymentChooseCreditDebitCardPage;
  let fixture: ComponentFixture<PaymentChooseCreditDebitCardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentChooseCreditDebitCardPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentChooseCreditDebitCardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
