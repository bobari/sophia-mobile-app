import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustCashVoucherDetailsNotEnoughPage } from './cust-cash-voucher-details-not-enough.page';

describe('CustCashVoucherDetailsNotEnoughPage', () => {
  let component: CustCashVoucherDetailsNotEnoughPage;
  let fixture: ComponentFixture<CustCashVoucherDetailsNotEnoughPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustCashVoucherDetailsNotEnoughPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustCashVoucherDetailsNotEnoughPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
