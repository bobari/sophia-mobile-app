import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AllProductsSearchWithinPage } from './all-products-search-within.page';

const routes: Routes = [
  {
    path: '',
    component: AllProductsSearchWithinPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AllProductsSearchWithinPage]
})
export class AllProductsSearchWithinPageModule {}
