import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustApptHistoryDetailsCancelledPage } from './cust-appt-history-details-cancelled.page';

const routes: Routes = [
  {
    path: '',
    component: CustApptHistoryDetailsCancelledPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustApptHistoryDetailsCancelledPage]
})
export class CustApptHistoryDetailsCancelledPageModule {}
