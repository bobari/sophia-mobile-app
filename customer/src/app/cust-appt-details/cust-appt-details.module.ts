import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustApptDetailsPage } from './cust-appt-details.page';

const routes: Routes = [
  {
    path: '',
    component: CustApptDetailsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustApptDetailsPage]
})
export class CustApptDetailsPageModule {}
