import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustSearchResultsPage } from './cust-search-results.page';

const routes: Routes = [
  {
    path: '',
    component: CustSearchResultsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustSearchResultsPage]
})
export class CustSearchResultsPageModule {}
