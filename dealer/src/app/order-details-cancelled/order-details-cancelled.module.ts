import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { OrderDetailsCancelledPage } from './order-details-cancelled.page';

const routes: Routes = [
  {
    path: '',
    component: OrderDetailsCancelledPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [OrderDetailsCancelledPage]
})
export class OrderDetailsCancelledPageModule {}
