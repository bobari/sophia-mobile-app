import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustNewApptThreeWaitinglistPage } from './cust-new-appt-three-waitinglist.page';

describe('CustNewApptThreeWaitinglistPage', () => {
  let component: CustNewApptThreeWaitinglistPage;
  let fixture: ComponentFixture<CustNewApptThreeWaitinglistPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustNewApptThreeWaitinglistPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustNewApptThreeWaitinglistPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
