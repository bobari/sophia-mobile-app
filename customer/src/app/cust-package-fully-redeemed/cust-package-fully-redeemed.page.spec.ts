import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustPackageFullyRedeemedPage } from './cust-package-fully-redeemed.page';

describe('CustPackageFullyRedeemedPage', () => {
  let component: CustPackageFullyRedeemedPage;
  let fixture: ComponentFixture<CustPackageFullyRedeemedPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustPackageFullyRedeemedPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustPackageFullyRedeemedPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
