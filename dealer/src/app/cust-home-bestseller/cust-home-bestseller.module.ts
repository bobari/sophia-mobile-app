import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustHomeBestsellerPage } from './cust-home-bestseller.page';

const routes: Routes = [
  {
    path: '',
    component: CustHomeBestsellerPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustHomeBestsellerPage]
})
export class CustHomeBestsellerPageModule {}
