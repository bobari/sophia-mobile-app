import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { OrderDetailsDeliveryPage } from './order-details-delivery.page';

const routes: Routes = [
  {
    path: '',
    component: OrderDetailsDeliveryPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [OrderDetailsDeliveryPage]
})
export class OrderDetailsDeliveryPageModule {}
