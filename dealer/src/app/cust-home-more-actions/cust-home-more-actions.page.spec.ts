import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustHomeMoreActionsPage } from './cust-home-more-actions.page';

describe('CustHomeMoreActionsPage', () => {
  let component: CustHomeMoreActionsPage;
  let fixture: ComponentFixture<CustHomeMoreActionsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustHomeMoreActionsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustHomeMoreActionsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
