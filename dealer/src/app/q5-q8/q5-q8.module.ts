import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { Q5Q8Page } from './q5-q8.page';

const routes: Routes = [
  {
    path: '',
    component: Q5Q8Page
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [Q5Q8Page]
})
export class Q5Q8PageModule {}
