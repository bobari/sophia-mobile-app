import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustBeautySolutionsNewskinrecordPage } from './cust-beauty-solutions-newskinrecord.page';

describe('CustBeautySolutionsNewskinrecordPage', () => {
  let component: CustBeautySolutionsNewskinrecordPage;
  let fixture: ComponentFixture<CustBeautySolutionsNewskinrecordPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustBeautySolutionsNewskinrecordPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustBeautySolutionsNewskinrecordPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
