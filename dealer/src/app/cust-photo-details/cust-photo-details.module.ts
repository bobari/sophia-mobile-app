import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustPhotoDetailsPage } from './cust-photo-details.page';

const routes: Routes = [
  {
    path: '',
    component: CustPhotoDetailsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustPhotoDetailsPage]
})
export class CustPhotoDetailsPageModule {}
