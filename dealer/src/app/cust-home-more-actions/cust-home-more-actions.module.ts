import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustHomeMoreActionsPage } from './cust-home-more-actions.page';

const routes: Routes = [
  {
    path: '',
    component: CustHomeMoreActionsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustHomeMoreActionsPage]
})
export class CustHomeMoreActionsPageModule {}
