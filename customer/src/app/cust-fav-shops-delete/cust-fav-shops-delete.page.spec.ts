import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustFavShopsDeletePage } from './cust-fav-shops-delete.page';

describe('CustFavShopsDeletePage', () => {
  let component: CustFavShopsDeletePage;
  let fixture: ComponentFixture<CustFavShopsDeletePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustFavShopsDeletePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustFavShopsDeletePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
