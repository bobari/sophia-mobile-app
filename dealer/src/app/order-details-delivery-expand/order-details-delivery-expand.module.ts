import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { OrderDetailsDeliveryExpandPage } from './order-details-delivery-expand.page';

const routes: Routes = [
  {
    path: '',
    component: OrderDetailsDeliveryExpandPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [OrderDetailsDeliveryExpandPage]
})
export class OrderDetailsDeliveryExpandPageModule {}
