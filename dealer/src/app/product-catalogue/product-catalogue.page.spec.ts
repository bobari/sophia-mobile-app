import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductCataloguePage } from './product-catalogue.page';

describe('ProductCataloguePage', () => {
  let component: ProductCataloguePage;
  let fixture: ComponentFixture<ProductCataloguePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductCataloguePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductCataloguePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
