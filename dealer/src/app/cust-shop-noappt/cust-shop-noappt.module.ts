import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustShopNoapptPage } from './cust-shop-noappt.page';

const routes: Routes = [
  {
    path: '',
    component: CustShopNoapptPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustShopNoapptPage]
})
export class CustShopNoapptPageModule {}
