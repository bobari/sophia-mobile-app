import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustApptReviewPage } from './cust-appt-review.page';

describe('CustApptReviewPage', () => {
  let component: CustApptReviewPage;
  let fixture: ComponentFixture<CustApptReviewPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustApptReviewPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustApptReviewPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
