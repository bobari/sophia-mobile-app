import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustMyVouchersUsedDetailsPage } from './cust-my-vouchers-used-details.page';

describe('CustMyVouchersUsedDetailsPage', () => {
  let component: CustMyVouchersUsedDetailsPage;
  let fixture: ComponentFixture<CustMyVouchersUsedDetailsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustMyVouchersUsedDetailsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustMyVouchersUsedDetailsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
