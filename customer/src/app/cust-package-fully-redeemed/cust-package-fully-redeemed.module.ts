import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustPackageFullyRedeemedPage } from './cust-package-fully-redeemed.page';

const routes: Routes = [
  {
    path: '',
    component: CustPackageFullyRedeemedPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustPackageFullyRedeemedPage]
})
export class CustPackageFullyRedeemedPageModule {}
