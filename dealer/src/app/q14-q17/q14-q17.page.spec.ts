import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Q14Q17Page } from './q14-q17.page';

describe('Q14Q17Page', () => {
  let component: Q14Q17Page;
  let fixture: ComponentFixture<Q14Q17Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Q14Q17Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Q14Q17Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
