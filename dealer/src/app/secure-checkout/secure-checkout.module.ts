import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SecureCheckoutPage } from './secure-checkout.page';

const routes: Routes = [
  {
    path: '',
    component: SecureCheckoutPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SecureCheckoutPage]
})
export class SecureCheckoutPageModule {}
