import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SummarySkinProblemsPage } from './summary-skin-problems.page';

describe('SummarySkinProblemsPage', () => {
  let component: SummarySkinProblemsPage;
  let fixture: ComponentFixture<SummarySkinProblemsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SummarySkinProblemsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SummarySkinProblemsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
