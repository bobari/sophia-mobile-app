import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustApptHistoryDetailsRedeemedPage } from './cust-appt-history-details-redeemed.page';

describe('CustApptHistoryDetailsRedeemedPage', () => {
  let component: CustApptHistoryDetailsRedeemedPage;
  let fixture: ComponentFixture<CustApptHistoryDetailsRedeemedPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustApptHistoryDetailsRedeemedPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustApptHistoryDetailsRedeemedPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
