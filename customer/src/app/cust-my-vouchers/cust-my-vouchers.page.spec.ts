import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustMyVouchersPage } from './cust-my-vouchers.page';

describe('CustMyVouchersPage', () => {
  let component: CustMyVouchersPage;
  let fixture: ComponentFixture<CustMyVouchersPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustMyVouchersPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustMyVouchersPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
