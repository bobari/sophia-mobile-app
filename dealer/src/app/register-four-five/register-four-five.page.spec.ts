import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterFourFivePage } from './register-four-five.page';

describe('RegisterFourFivePage', () => {
  let component: RegisterFourFivePage;
  let fixture: ComponentFixture<RegisterFourFivePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterFourFivePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterFourFivePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
