import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AllProductsListViewPage } from './all-products-list-view.page';

const routes: Routes = [
  {
    path: '',
    component: AllProductsListViewPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AllProductsListViewPage]
})
export class AllProductsListViewPageModule {}
