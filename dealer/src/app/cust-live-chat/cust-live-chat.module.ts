import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustLiveChatPage } from './cust-live-chat.page';

const routes: Routes = [
  {
    path: '',
    component: CustLiveChatPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustLiveChatPage]
})
export class CustLiveChatPageModule {}
