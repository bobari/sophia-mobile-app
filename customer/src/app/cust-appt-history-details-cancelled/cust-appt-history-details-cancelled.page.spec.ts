import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustApptHistoryDetailsCancelledPage } from './cust-appt-history-details-cancelled.page';

describe('CustApptHistoryDetailsCancelledPage', () => {
  let component: CustApptHistoryDetailsCancelledPage;
  let fixture: ComponentFixture<CustApptHistoryDetailsCancelledPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustApptHistoryDetailsCancelledPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustApptHistoryDetailsCancelledPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
