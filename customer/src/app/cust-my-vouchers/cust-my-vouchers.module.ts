import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustMyVouchersPage } from './cust-my-vouchers.page';

const routes: Routes = [
  {
    path: '',
    component: CustMyVouchersPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustMyVouchersPage]
})
export class CustMyVouchersPageModule {}
