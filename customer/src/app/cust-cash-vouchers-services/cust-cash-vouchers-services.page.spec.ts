import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustCashVouchersServicesPage } from './cust-cash-vouchers-services.page';

describe('CustCashVouchersServicesPage', () => {
  let component: CustCashVouchersServicesPage;
  let fixture: ComponentFixture<CustCashVouchersServicesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustCashVouchersServicesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustCashVouchersServicesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
