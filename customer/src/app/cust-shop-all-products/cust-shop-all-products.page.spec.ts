import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustShopAllProductsPage } from './cust-shop-all-products.page';

describe('CustShopAllProductsPage', () => {
  let component: CustShopAllProductsPage;
  let fixture: ComponentFixture<CustShopAllProductsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustShopAllProductsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustShopAllProductsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
