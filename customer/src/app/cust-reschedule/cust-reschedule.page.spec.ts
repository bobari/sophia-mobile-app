import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustReschedulePage } from './cust-reschedule.page';

describe('CustReschedulePage', () => {
  let component: CustReschedulePage;
  let fixture: ComponentFixture<CustReschedulePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustReschedulePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustReschedulePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
