import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustRedemptionHistoryPage } from './cust-redemption-history.page';

const routes: Routes = [
  {
    path: '',
    component: CustRedemptionHistoryPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustRedemptionHistoryPage]
})
export class CustRedemptionHistoryPageModule {}
