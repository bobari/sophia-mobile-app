import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustSetRemindersPage } from './cust-set-reminders.page';

const routes: Routes = [
  {
    path: '',
    component: CustSetRemindersPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustSetRemindersPage]
})
export class CustSetRemindersPageModule {}
