import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustPurchasedPackagesPage } from './cust-purchased-packages.page';

const routes: Routes = [
  {
    path: '',
    component: CustPurchasedPackagesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustPurchasedPackagesPage]
})
export class CustPurchasedPackagesPageModule {}
