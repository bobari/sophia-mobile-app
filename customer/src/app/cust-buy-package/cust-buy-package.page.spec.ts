import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustBuyPackagePage } from './cust-buy-package.page';

describe('CustBuyPackagePage', () => {
  let component: CustBuyPackagePage;
  let fixture: ComponentFixture<CustBuyPackagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustBuyPackagePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustBuyPackagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
