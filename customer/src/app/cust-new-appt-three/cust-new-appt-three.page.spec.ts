import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustNewApptThreePage } from './cust-new-appt-three.page';

describe('CustNewApptThreePage', () => {
  let component: CustNewApptThreePage;
  let fixture: ComponentFixture<CustNewApptThreePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustNewApptThreePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustNewApptThreePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
