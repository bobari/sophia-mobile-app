import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PastApptDetailsPage } from './past-appt-details.page';

const routes: Routes = [
  {
    path: '',
    component: PastApptDetailsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PastApptDetailsPage]
})
export class PastApptDetailsPageModule {}
