import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustMyVouchersUsedPage } from './cust-my-vouchers-used.page';

describe('CustMyVouchersUsedPage', () => {
  let component: CustMyVouchersUsedPage;
  let fixture: ComponentFixture<CustMyVouchersUsedPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustMyVouchersUsedPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustMyVouchersUsedPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
