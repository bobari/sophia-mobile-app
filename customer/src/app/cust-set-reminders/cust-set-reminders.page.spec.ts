import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustSetRemindersPage } from './cust-set-reminders.page';

describe('CustSetRemindersPage', () => {
  let component: CustSetRemindersPage;
  let fixture: ComponentFixture<CustSetRemindersPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustSetRemindersPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustSetRemindersPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
