import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PaymentCreditDebitCardPage } from './payment-credit-debit-card.page';

const routes: Routes = [
  {
    path: '',
    component: PaymentCreditDebitCardPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PaymentCreditDebitCardPage]
})
export class PaymentCreditDebitCardPageModule {}
