import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustCashVouchersProductsPage } from './cust-cash-vouchers-products.page';

const routes: Routes = [
  {
    path: '',
    component: CustCashVouchersProductsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustCashVouchersProductsPage]
})
export class CustCashVouchersProductsPageModule {}
