import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckoutShippingDeliveryApplyCodePage } from './checkout-shipping-delivery-apply-code.page';

describe('CheckoutShippingDeliveryApplyCodePage', () => {
  let component: CheckoutShippingDeliveryApplyCodePage;
  let fixture: ComponentFixture<CheckoutShippingDeliveryApplyCodePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckoutShippingDeliveryApplyCodePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckoutShippingDeliveryApplyCodePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
