import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustBuyPackagePage } from './cust-buy-package.page';

const routes: Routes = [
  {
    path: '',
    component: CustBuyPackagePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustBuyPackagePage]
})
export class CustBuyPackagePageModule {}
