import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CheckoutShippingSelfCollectPage } from './checkout-shipping-self-collect.page';

const routes: Routes = [
  {
    path: '',
    component: CheckoutShippingSelfCollectPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CheckoutShippingSelfCollectPage]
})
export class CheckoutShippingSelfCollectPageModule {}
