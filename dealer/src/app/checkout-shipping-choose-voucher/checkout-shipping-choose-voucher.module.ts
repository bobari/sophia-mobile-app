import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CheckoutShippingChooseVoucherPage } from './checkout-shipping-choose-voucher.page';

const routes: Routes = [
  {
    path: '',
    component: CheckoutShippingChooseVoucherPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CheckoutShippingChooseVoucherPage]
})
export class CheckoutShippingChooseVoucherPageModule {}
