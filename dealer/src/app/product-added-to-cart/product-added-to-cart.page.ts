import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product-added-to-cart',
  templateUrl: './product-added-to-cart.page.html',
  styleUrls: ['./product-added-to-cart.page.scss'],
})
export class ProductAddedToCartPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
