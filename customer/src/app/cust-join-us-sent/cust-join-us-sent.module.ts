import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustJoinUsSentPage } from './cust-join-us-sent.page';

const routes: Routes = [
  {
    path: '',
    component: CustJoinUsSentPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustJoinUsSentPage]
})
export class CustJoinUsSentPageModule {}
