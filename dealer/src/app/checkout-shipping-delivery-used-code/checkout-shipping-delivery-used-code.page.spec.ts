import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckoutShippingDeliveryUsedCodePage } from './checkout-shipping-delivery-used-code.page';

describe('CheckoutShippingDeliveryUsedCodePage', () => {
  let component: CheckoutShippingDeliveryUsedCodePage;
  let fixture: ComponentFixture<CheckoutShippingDeliveryUsedCodePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckoutShippingDeliveryUsedCodePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckoutShippingDeliveryUsedCodePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
