import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ProductSearchWithinPage } from './product-search-within.page';

const routes: Routes = [
  {
    path: '',
    component: ProductSearchWithinPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ProductSearchWithinPage]
})
export class ProductSearchWithinPageModule {}
