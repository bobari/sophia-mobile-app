import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustUpcomingApptPage } from './cust-upcoming-appt.page';

describe('CustUpcomingApptPage', () => {
  let component: CustUpcomingApptPage;
  let fixture: ComponentFixture<CustUpcomingApptPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustUpcomingApptPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustUpcomingApptPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
