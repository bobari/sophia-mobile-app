import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustBeautySolutionsNoskinrecordPage } from './cust-beauty-solutions-noskinrecord.page';

const routes: Routes = [
  {
    path: '',
    component: CustBeautySolutionsNoskinrecordPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustBeautySolutionsNoskinrecordPage]
})
export class CustBeautySolutionsNoskinrecordPageModule {}
