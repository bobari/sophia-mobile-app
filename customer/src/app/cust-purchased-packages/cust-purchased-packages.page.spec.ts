import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustPurchasedPackagesPage } from './cust-purchased-packages.page';

describe('CustPurchasedPackagesPage', () => {
  let component: CustPurchasedPackagesPage;
  let fixture: ComponentFixture<CustPurchasedPackagesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustPurchasedPackagesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustPurchasedPackagesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
