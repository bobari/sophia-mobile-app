import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductSimilarPage } from './product-similar.page';

describe('ProductSimilarPage', () => {
  let component: ProductSimilarPage;
  let fixture: ComponentFixture<ProductSimilarPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductSimilarPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductSimilarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
