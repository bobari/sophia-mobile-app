import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustNewApptDetailsPage } from './cust-new-appt-details.page';

const routes: Routes = [
  {
    path: '',
    component: CustNewApptDetailsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustNewApptDetailsPage]
})
export class CustNewApptDetailsPageModule {}
