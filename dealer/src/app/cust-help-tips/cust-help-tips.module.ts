import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustHelpTipsPage } from './cust-help-tips.page';

const routes: Routes = [
  {
    path: '',
    component: CustHelpTipsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustHelpTipsPage]
})
export class CustHelpTipsPageModule {}
