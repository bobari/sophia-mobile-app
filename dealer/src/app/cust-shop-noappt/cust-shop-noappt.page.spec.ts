import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustShopNoapptPage } from './cust-shop-noappt.page';

describe('CustShopNoapptPage', () => {
  let component: CustShopNoapptPage;
  let fixture: ComponentFixture<CustShopNoapptPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustShopNoapptPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustShopNoapptPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
