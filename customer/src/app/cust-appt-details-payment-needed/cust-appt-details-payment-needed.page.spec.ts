import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustApptDetailsPaymentNeededPage } from './cust-appt-details-payment-needed.page';

describe('CustApptDetailsPaymentNeededPage', () => {
  let component: CustApptDetailsPaymentNeededPage;
  let fixture: ComponentFixture<CustApptDetailsPaymentNeededPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustApptDetailsPaymentNeededPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustApptDetailsPaymentNeededPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
