import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustHomeBestsellerPage } from './cust-home-bestseller.page';

describe('CustHomeBestsellerPage', () => {
  let component: CustHomeBestsellerPage;
  let fixture: ComponentFixture<CustHomeBestsellerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustHomeBestsellerPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustHomeBestsellerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
