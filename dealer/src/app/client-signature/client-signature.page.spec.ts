import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientSignaturePage } from './client-signature.page';

describe('ClientSignaturePage', () => {
  let component: ClientSignaturePage;
  let fixture: ComponentFixture<ClientSignaturePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientSignaturePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientSignaturePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
