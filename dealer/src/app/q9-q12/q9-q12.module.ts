import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { Q9Q12Page } from './q9-q12.page';

const routes: Routes = [
  {
    path: '',
    component: Q9Q12Page
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [Q9Q12Page]
})
export class Q9Q12PageModule {}
