import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustBeautySolutionsPage } from './cust-beauty-solutions.page';

describe('CustBeautySolutionsPage', () => {
  let component: CustBeautySolutionsPage;
  let fixture: ComponentFixture<CustBeautySolutionsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustBeautySolutionsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustBeautySolutionsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
