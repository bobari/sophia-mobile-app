import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Q13Page } from './q13.page';

describe('Q13Page', () => {
  let component: Q13Page;
  let fixture: ComponentFixture<Q13Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Q13Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Q13Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
