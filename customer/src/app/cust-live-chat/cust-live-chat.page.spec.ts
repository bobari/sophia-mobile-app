import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustLiveChatPage } from './cust-live-chat.page';

describe('CustLiveChatPage', () => {
  let component: CustLiveChatPage;
  let fixture: ComponentFixture<CustLiveChatPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustLiveChatPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustLiveChatPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
