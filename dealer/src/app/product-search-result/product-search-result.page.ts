import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product-search-result',
  templateUrl: './product-search-result.page.html',
  styleUrls: ['./product-search-result.page.scss'],
})
export class ProductSearchResultPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
