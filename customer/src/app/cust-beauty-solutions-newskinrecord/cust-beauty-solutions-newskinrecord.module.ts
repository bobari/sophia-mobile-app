import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustBeautySolutionsNewskinrecordPage } from './cust-beauty-solutions-newskinrecord.page';

const routes: Routes = [
  {
    path: '',
    component: CustBeautySolutionsNewskinrecordPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustBeautySolutionsNewskinrecordPage]
})
export class CustBeautySolutionsNewskinrecordPageModule {}
