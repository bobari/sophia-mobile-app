import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RegisterFaceFilledPage } from './register-face-filled.page';

const routes: Routes = [
  {
    path: '',
    component: RegisterFaceFilledPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RegisterFaceFilledPage]
})
export class RegisterFaceFilledPageModule {}
