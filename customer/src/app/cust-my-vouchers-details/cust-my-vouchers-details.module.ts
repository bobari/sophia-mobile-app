import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustMyVouchersDetailsPage } from './cust-my-vouchers-details.page';

const routes: Routes = [
  {
    path: '',
    component: CustMyVouchersDetailsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustMyVouchersDetailsPage]
})
export class CustMyVouchersDetailsPageModule {}
