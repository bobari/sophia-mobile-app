import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CheckoutShippingDeliveryUsedVoucherPage } from './checkout-shipping-delivery-used-voucher.page';

const routes: Routes = [
  {
    path: '',
    component: CheckoutShippingDeliveryUsedVoucherPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CheckoutShippingDeliveryUsedVoucherPage]
})
export class CheckoutShippingDeliveryUsedVoucherPageModule {}
