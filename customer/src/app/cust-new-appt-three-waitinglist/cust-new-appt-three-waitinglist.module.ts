import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustNewApptThreeWaitinglistPage } from './cust-new-appt-three-waitinglist.page';

const routes: Routes = [
  {
    path: '',
    component: CustNewApptThreeWaitinglistPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustNewApptThreeWaitinglistPage]
})
export class CustNewApptThreeWaitinglistPageModule {}
