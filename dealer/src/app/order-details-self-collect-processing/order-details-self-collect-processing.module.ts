import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { OrderDetailsSelfCollectProcessingPage } from './order-details-self-collect-processing.page';

const routes: Routes = [
  {
    path: '',
    component: OrderDetailsSelfCollectProcessingPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [OrderDetailsSelfCollectProcessingPage]
})
export class OrderDetailsSelfCollectProcessingPageModule {}
