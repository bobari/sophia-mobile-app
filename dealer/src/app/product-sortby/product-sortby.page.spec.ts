import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductSortbyPage } from './product-sortby.page';

describe('ProductSortbyPage', () => {
  let component: ProductSortbyPage;
  let fixture: ComponentFixture<ProductSortbyPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductSortbyPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductSortbyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
