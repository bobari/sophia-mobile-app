import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CheckoutShippingDeliveryUsedCodePage } from './checkout-shipping-delivery-used-code.page';

const routes: Routes = [
  {
    path: '',
    component: CheckoutShippingDeliveryUsedCodePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CheckoutShippingDeliveryUsedCodePage]
})
export class CheckoutShippingDeliveryUsedCodePageModule {}
