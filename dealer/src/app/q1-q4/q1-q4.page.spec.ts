import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Q1Q4Page } from './q1-q4.page';

describe('Q1Q4Page', () => {
  let component: Q1Q4Page;
  let fixture: ComponentFixture<Q1Q4Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Q1Q4Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Q1Q4Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
