import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustSetLanguagePage } from './cust-set-language.page';

describe('CustSetLanguagePage', () => {
  let component: CustSetLanguagePage;
  let fixture: ComponentFixture<CustSetLanguagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustSetLanguagePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustSetLanguagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
