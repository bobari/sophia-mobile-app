import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderDetailsDeliveryExpandPage } from './order-details-delivery-expand.page';

describe('OrderDetailsDeliveryExpandPage', () => {
  let component: OrderDetailsDeliveryExpandPage;
  let fixture: ComponentFixture<OrderDetailsDeliveryExpandPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderDetailsDeliveryExpandPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderDetailsDeliveryExpandPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
