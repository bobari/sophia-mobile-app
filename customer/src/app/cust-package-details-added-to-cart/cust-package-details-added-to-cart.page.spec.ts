import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustPackageDetailsAddedToCartPage } from './cust-package-details-added-to-cart.page';

describe('CustPackageDetailsAddedToCartPage', () => {
  let component: CustPackageDetailsAddedToCartPage;
  let fixture: ComponentFixture<CustPackageDetailsAddedToCartPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustPackageDetailsAddedToCartPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustPackageDetailsAddedToCartPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
