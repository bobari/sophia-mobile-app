import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckoutPaymentUsePointsCompletelyPage } from './checkout-payment-use-points-completely.page';

describe('CheckoutPaymentUsePointsCompletelyPage', () => {
  let component: CheckoutPaymentUsePointsCompletelyPage;
  let fixture: ComponentFixture<CheckoutPaymentUsePointsCompletelyPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckoutPaymentUsePointsCompletelyPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckoutPaymentUsePointsCompletelyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
