import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustNearMePage } from './cust-near-me.page';

describe('CustNearMePage', () => {
  let component: CustNearMePage;
  let fixture: ComponentFixture<CustNearMePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustNearMePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustNearMePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
