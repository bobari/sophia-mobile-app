import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustFavShopsDeletePage } from './cust-fav-shops-delete.page';

const routes: Routes = [
  {
    path: '',
    component: CustFavShopsDeletePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustFavShopsDeletePage]
})
export class CustFavShopsDeletePageModule {}
