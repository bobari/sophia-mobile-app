import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApptDetailsPaymentNeededPage } from './appt-details-payment-needed.page';

describe('ApptDetailsPaymentNeededPage', () => {
  let component: ApptDetailsPaymentNeededPage;
  let fixture: ComponentFixture<ApptDetailsPaymentNeededPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApptDetailsPaymentNeededPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApptDetailsPaymentNeededPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
