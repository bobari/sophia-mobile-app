import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustNewsNotificationsPage } from './cust-news-notifications.page';

const routes: Routes = [
  {
    path: '',
    component: CustNewsNotificationsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustNewsNotificationsPage]
})
export class CustNewsNotificationsPageModule {}
