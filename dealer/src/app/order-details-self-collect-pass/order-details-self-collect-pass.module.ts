import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { OrderDetailsSelfCollectPassPage } from './order-details-self-collect-pass.page';

const routes: Routes = [
  {
    path: '',
    component: OrderDetailsSelfCollectPassPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [OrderDetailsSelfCollectPassPage]
})
export class OrderDetailsSelfCollectPassPageModule {}
