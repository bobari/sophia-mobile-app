import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderDetailsSelfCollectClosedPage } from './order-details-self-collect-closed.page';

describe('OrderDetailsSelfCollectClosedPage', () => {
  let component: OrderDetailsSelfCollectClosedPage;
  let fixture: ComponentFixture<OrderDetailsSelfCollectClosedPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderDetailsSelfCollectClosedPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderDetailsSelfCollectClosedPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
