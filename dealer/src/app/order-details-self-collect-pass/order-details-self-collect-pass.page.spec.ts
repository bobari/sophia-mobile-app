import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderDetailsSelfCollectPassPage } from './order-details-self-collect-pass.page';

describe('OrderDetailsSelfCollectPassPage', () => {
  let component: OrderDetailsSelfCollectPassPage;
  let fixture: ComponentFixture<OrderDetailsSelfCollectPassPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderDetailsSelfCollectPassPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderDetailsSelfCollectPassPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
