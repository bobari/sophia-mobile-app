import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustHelpTipsPage } from './cust-help-tips.page';

describe('CustHelpTipsPage', () => {
  let component: CustHelpTipsPage;
  let fixture: ComponentFixture<CustHelpTipsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustHelpTipsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustHelpTipsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
