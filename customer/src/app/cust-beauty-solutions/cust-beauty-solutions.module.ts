import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustBeautySolutionsPage } from './cust-beauty-solutions.page';

const routes: Routes = [
  {
    path: '',
    component: CustBeautySolutionsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustBeautySolutionsPage]
})
export class CustBeautySolutionsPageModule {}
