import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterThreeFivePage } from './register-three-five.page';

describe('RegisterThreeFivePage', () => {
  let component: RegisterThreeFivePage;
  let fixture: ComponentFixture<RegisterThreeFivePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterThreeFivePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterThreeFivePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
