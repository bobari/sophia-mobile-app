import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerVipPage } from './customer-vip.page';

describe('CustomerVipPage', () => {
  let component: CustomerVipPage;
  let fixture: ComponentFixture<CustomerVipPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerVipPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerVipPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
