import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustApptReviewedPage } from './cust-appt-reviewed.page';

const routes: Routes = [
  {
    path: '',
    component: CustApptReviewedPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustApptReviewedPage]
})
export class CustApptReviewedPageModule {}
