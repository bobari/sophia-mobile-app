import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustNewsNotificationsDetailsPage } from './cust-news-notifications-details.page';

const routes: Routes = [
  {
    path: '',
    component: CustNewsNotificationsDetailsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustNewsNotificationsDetailsPage]
})
export class CustNewsNotificationsDetailsPageModule {}
