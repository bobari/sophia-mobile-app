import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CheckoutPaymentUsePointsCompletelyPage } from './checkout-payment-use-points-completely.page';

const routes: Routes = [
  {
    path: '',
    component: CheckoutPaymentUsePointsCompletelyPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CheckoutPaymentUsePointsCompletelyPage]
})
export class CheckoutPaymentUsePointsCompletelyPageModule {}
