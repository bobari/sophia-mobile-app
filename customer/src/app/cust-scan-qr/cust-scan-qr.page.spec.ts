import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustScanQrPage } from './cust-scan-qr.page';

describe('CustScanQrPage', () => {
  let component: CustScanQrPage;
  let fixture: ComponentFixture<CustScanQrPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustScanQrPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustScanQrPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
