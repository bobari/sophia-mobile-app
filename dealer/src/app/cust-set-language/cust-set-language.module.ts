import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustSetLanguagePage } from './cust-set-language.page';

const routes: Routes = [
  {
    path: '',
    component: CustSetLanguagePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustSetLanguagePage]
})
export class CustSetLanguagePageModule {}
