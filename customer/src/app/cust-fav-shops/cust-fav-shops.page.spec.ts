import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustFavShopsPage } from './cust-fav-shops.page';

describe('CustFavShopsPage', () => {
  let component: CustFavShopsPage;
  let fixture: ComponentFixture<CustFavShopsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustFavShopsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustFavShopsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
