import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HealthConditionPage } from './health-condition.page';

describe('HealthConditionPage', () => {
  let component: HealthConditionPage;
  let fixture: ComponentFixture<HealthConditionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HealthConditionPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HealthConditionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
