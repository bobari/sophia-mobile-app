import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustShopPage } from './cust-shop.page';

describe('CustShopPage', () => {
  let component: CustShopPage;
  let fixture: ComponentFixture<CustShopPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustShopPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustShopPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
