import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustCashVouchersProductsPage } from './cust-cash-vouchers-products.page';

describe('CustCashVouchersProductsPage', () => {
  let component: CustCashVouchersProductsPage;
  let fixture: ComponentFixture<CustCashVouchersProductsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustCashVouchersProductsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustCashVouchersProductsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
