import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustApptBeauticanDetailsPage } from './cust-appt-beautican-details.page';

const routes: Routes = [
  {
    path: '',
    component: CustApptBeauticanDetailsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustApptBeauticanDetailsPage]
})
export class CustApptBeauticanDetailsPageModule {}
