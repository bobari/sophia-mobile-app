import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Q9Q12Page } from './q9-q12.page';

describe('Q9Q12Page', () => {
  let component: Q9Q12Page;
  let fixture: ComponentFixture<Q9Q12Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Q9Q12Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Q9Q12Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
