import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustSettingsHelpTipsResetPage } from './cust-settings-help-tips-reset.page';

describe('CustSettingsHelpTipsResetPage', () => {
  let component: CustSettingsHelpTipsResetPage;
  let fixture: ComponentFixture<CustSettingsHelpTipsResetPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustSettingsHelpTipsResetPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustSettingsHelpTipsResetPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
