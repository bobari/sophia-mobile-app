import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustNewApptSummaryPage } from './cust-new-appt-summary.page';

const routes: Routes = [
  {
    path: '',
    component: CustNewApptSummaryPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustNewApptSummaryPage]
})
export class CustNewApptSummaryPageModule {}
