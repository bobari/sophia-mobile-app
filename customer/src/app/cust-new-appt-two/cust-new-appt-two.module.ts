import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustNewApptTwoPage } from './cust-new-appt-two.page';

const routes: Routes = [
  {
    path: '',
    component: CustNewApptTwoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustNewApptTwoPage]
})
export class CustNewApptTwoPageModule {}
