import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustGalleryPage } from './cust-gallery.page';

describe('CustGalleryPage', () => {
  let component: CustGalleryPage;
  let fixture: ComponentFixture<CustGalleryPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustGalleryPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustGalleryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
