import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustCashVoucherDetailsNotEnoughPage } from './cust-cash-voucher-details-not-enough.page';

const routes: Routes = [
  {
    path: '',
    component: CustCashVoucherDetailsNotEnoughPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustCashVoucherDetailsNotEnoughPage]
})
export class CustCashVoucherDetailsNotEnoughPageModule {}
