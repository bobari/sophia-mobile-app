import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductAddToCartPage } from './product-add-to-cart.page';

describe('ProductAddToCartPage', () => {
  let component: ProductAddToCartPage;
  let fixture: ComponentFixture<ProductAddToCartPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductAddToCartPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductAddToCartPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
