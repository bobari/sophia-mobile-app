import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustJoinUsSentPage } from './cust-join-us-sent.page';

describe('CustJoinUsSentPage', () => {
  let component: CustJoinUsSentPage;
  let fixture: ComponentFixture<CustJoinUsSentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustJoinUsSentPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustJoinUsSentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
