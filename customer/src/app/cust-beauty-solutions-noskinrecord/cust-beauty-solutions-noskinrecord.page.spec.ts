import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustBeautySolutionsNoskinrecordPage } from './cust-beauty-solutions-noskinrecord.page';

describe('CustBeautySolutionsNoskinrecordPage', () => {
  let component: CustBeautySolutionsNoskinrecordPage;
  let fixture: ComponentFixture<CustBeautySolutionsNoskinrecordPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustBeautySolutionsNoskinrecordPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustBeautySolutionsNoskinrecordPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
