import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustContactUsPage } from './cust-contact-us.page';

describe('CustContactUsPage', () => {
  let component: CustContactUsPage;
  let fixture: ComponentFixture<CustContactUsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustContactUsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustContactUsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
