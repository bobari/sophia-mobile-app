import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CheckoutShippingDeliveryPage } from './checkout-shipping-delivery.page';

const routes: Routes = [
  {
    path: '',
    component: CheckoutShippingDeliveryPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CheckoutShippingDeliveryPage]
})
export class CheckoutShippingDeliveryPageModule {}
